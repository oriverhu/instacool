import React from 'react';
import { connect } from 'react-redux';

import Cards from './../../components/Cards';
import Container from './../../components/Container';
import Title from './../../components/Title';
import LoginForm from '../../components/LoginForm';

import { login as loginThunk,ILogin } from './../../ducks/Users';

interface ILoginProps{
 login: (a: ILogin) => void
}

class Login extends React.Component<ILoginProps>{
    public render(){
        const { login } = this.props
        return(
            <Container center>
                <Cards>
                    <Title>Iniciar Sesión</Title>                          
                    <LoginForm  onSubmit={login}/>
                </Cards>
            </Container>
        )
    }
}
const mapStateToProps = (state:any) => state;
const mapDispatchToProps = (dispatch:any) => ({
    login: (payload:any) => dispatch(loginThunk(payload))
})
export default connect(mapStateToProps, mapDispatchToProps)(Login)