import React from 'react';
import { connect } from 'react-redux';
import { register as registerThunk, ILogin } from './../../ducks/Users';
import { ThunkDispatch } from 'redux-thunk';

import Cards from './../../components/Cards';
import Container from './../../components/Container';
import Title from './../../components/Title';
import RegisterForm from '../../components/RegisterForm';


interface IRegisterProps {
    register: (a: ILogin) => void
}

class Regiter extends React.Component<IRegisterProps>{
    public render(){
        const { register } = this.props;
        return(
            <Container center>
                <Cards>
                    <Title>Registro</Title>          
                    <RegisterForm onSubmit={register} />
                </Cards>
            </Container>
        )
    }
}

const mapStateToProps = (state:any) => state;
const mapDispatchToProps = (dispatch:ThunkDispatch<any, any, any>) => ({
    register: (payload:any) => dispatch(registerThunk(payload))
})
export default connect(mapStateToProps, mapDispatchToProps)(Regiter)