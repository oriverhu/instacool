import React from 'react';

import { chunk } from 'lodash';

import { submit } from 'redux-form';

import ProfileImg from '../../components/ProfileImg';
import Button from '../../components/Button';
import Cards from '../../components/Cards';

import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import * as postsDuck from './../../ducks/Posts'
import * as usersDuck from './../../ducks/Users'
import { bindActionCreators } from 'redux';

import services from '../../services';
const { auth } = services;


const styles={
    container: {
        padding: '15px'
    },
    row: {
        display: 'flex',
        justifyContent: 'space-between',
        marginBottom: '10px'
    },
    img: {
        width: '100px'
    }
}

interface IProfileProps{
    fetchPosts: () => void,
    fetched: boolean,
    loading: boolean,
    data: postsDuck.IPost[][],
    submitProfileImg: () => void,
    // handleProfileImageSubmit: (a: { file:File }) => void,
    handleProfileImageSubmit:any,
    profileImage: string
}

class Profile extends React.Component <IProfileProps>{
// export default class Profile extends React.Component{

    constructor(props: IProfileProps){
        super(props)
        const { fetchPosts, fetched } = props
        if(fetched){
            return
        }
        fetchPosts()
    }

    public render(){
        const { data, submitProfileImg, handleProfileImageSubmit, profileImage } = this.props
        
        return(
            <div style={styles.container}>
                <div style={styles.row}>
                    <ProfileImg 
                        submitProfileImg={submitProfileImg} 
                        onSubmit={handleProfileImageSubmit} 
                        profileImage={profileImage} 
                     />
                    <Button>Agregar</Button>
                </div>

                {data.map((x,i)=>
                    <div key={i} style={styles.row}>
                        {x.map((y,i)=> 
                            <Cards key={i}><img style={styles.img} src={y.imageURL} alt={y.imageURL} /></Cards>
                        )} 
                    </div>
                )}

                {/*
                <div style={styles.row}>
                    <Cards><img src="http://placekitten.com/100/100" alt="Picture1" /></Cards>
                    <Cards><img src="http://placekitten.com/100/100" alt="Picture2" /></Cards>
                    <Cards><img src="http://placekitten.com/100/100" alt="Picture3" /></Cards>
                </div>
                <div style={styles.row}>
                    <Cards><img src="http://placekitten.com/100/100" alt="Picture4" /></Cards>
                    <Cards><img src="http://placekitten.com/100/100" alt="Picture5" /></Cards>
                    <Cards><img src="http://placekitten.com/100/100" alt="Picture6" /></Cards>
                </div> */}
            </div>
        )
    }
}

const mapStateToProps = (state:any) =>{
    const { Posts: {data, fetched, fetching } } = state
    const { Users: {profileImage: tempPI } } = state
    const profileImage = tempPI || "http://placekitten.com/100/100";
    const loading = fetching || !fetched

    const filtered = Object.keys(data).reduce((acc, el)=>{
        if(data[el].userId !== auth.currentUser?.uid){
            return acc;
        }
        return acc.concat(data[el])
    },[] as postsDuck.IPost[])

    return {
        loading,
        fetched,
        data: chunk(filtered,3),
        profileImage
        // data
    }
}
const mapDispatchToProps = (dispatch:ThunkDispatch<any, any, any>) => bindActionCreators({
    ...postsDuck,
    ...usersDuck,
    submitProfileImg: () => submit('profileImg')
}, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Profile)