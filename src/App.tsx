import React from 'react';
import './App.css';
import { Route } from 'react-router';
import { History } from 'history';

import Login from './containers/Auth/Login';
import Register from './containers/Auth/Register';
import NewsFeed from './containers/NewsFeed';
import Profile from './containers/Profile';
import Navbar from './components/Navbar';

import services from './services';

interface IApp {
  history: History,
  loadInitialData: () => void
}

class App extends React.Component<IApp>{

  public state = {
    loading: true
  }

  public componentDidMount(){
    const { auth } = services;

    auth.onAuthStateChanged( user => {
      // console.log(user);
      const { history } = this.props
      if(user){
            const { loadInitialData } = this.props
            loadInitialData()
           if(['/','/register'].indexOf(window.location.pathname) > -1){
            history.push('/app/newsfeed')
           }
      }else{
        // /app/cualquiercosa
        if(/\app\/./.test(window.location.pathname)){
          history.push('/')
        }
      } 
      this.setState({ loading: false })
    })
  }

  public render(){
    const { loading } = this.state
    return (
      loading ? 'Loading' : 
      <div>
        <Route exact path='/' component={Login} />
        <Route exact path='/register' component={Register} />
        <Route path='/app' component={Navbar} />
        <Route exact path='/app/newsfeed' component={NewsFeed} />
        <Route exact path='/app/profile' component={Profile} />
      </div>
    );
  }
}

export default App;
