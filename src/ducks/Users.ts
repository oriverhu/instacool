import { Dispatch } from "redux";
import { IServices } from './../services';
import { AnyAction } from '../../../instacool/anterior/redux/index';

export interface ILogin {
    email: string,
    password: string
}

const SET_PROFILE_IMG = 'users/set-profile-image'

export const setProfileImage = (payload:string) => ({
    type: SET_PROFILE_IMG,
    payload
})

export const loadUserInitialData = () => 
    async (dispatch: Dispatch, getState: ()=> any, { auth,storage }: IServices) => {
        if(!auth.currentUser){
            return
        }

        const { uid } = auth.currentUser
        const storageRef = storage.ref()

        const imageRef = await storageRef
            .child(`profileImages`)
            .child(`${uid}.jpg`)

        const url = await imageRef.getDownloadURL()

        dispatch(setProfileImage(url))
    } 

export default function reducer(state = {}, action: AnyAction){
    switch (action.type) {
        case SET_PROFILE_IMG:
            return {
                ...state,
                profileImage: action.payload
            }
    
        default:
           return state
    }
}

export const login = ({email, password}: ILogin) =>
async (dispatch: Dispatch, getState: ()=> any, { auth }: IServices) => {
     const result = await auth.signInWithEmailAndPassword(email, password)
     console.log(result);
}

export const register = ({email, password}: ILogin) =>
async (dispatch: Dispatch, getState: ()=> any, { auth, db  }: IServices) => {
    const userCredentials = await auth.createUserWithEmailAndPassword(email, password);
    const { user } = userCredentials;    
    const id = user ? user.uid : undefined;
    const doc = db.collection('users').doc(id)
    await doc.set({ role: 'user' })
}

export const handleProfileImageSubmit = (payload: {profileImg: File}) => 
    async (dispatch: Dispatch, getState: () => any, { auth, storage }: IServices) => {
    // tslint:disable-next-line:no-console
        if(!auth.currentUser){
            return
        }

        const { uid } = auth.currentUser
        const storageRef = storage.ref()

        const response = await storageRef
            .child(`profileImages`)
            .child(`${uid}.jpg`)
            .put(payload.profileImg)

        const url = await response.ref.getDownloadURL()  
        
        dispatch(setProfileImage(url))
    }