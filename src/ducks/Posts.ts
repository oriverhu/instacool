import { Dispatch, AnyAction } from 'redux'
import { IServices } from './../services'
import { download } from './../utils'
import { firestore } from 'firebase'
import { dispatch } from '../../../instacool/anterior/rxjs/src/internal/observable/range';

const START = '/posts/fetch-start'
const SUCCESS = '/posts/fetch-success'
const ERROR = '/posts/fetch-error'
const ADD = '/posts/add'

const fetchStart = () => ({
    type: START
})

export interface IPost {
    comment: string,
    userId: string,
    createdAt: firestore.Timestamp,
    imageURL: string
}

export interface IDataPosts{
    [key:string]: IPost
}
const fetchSuccess = (payload:IDataPosts) => ({
    payload,
    type: SUCCESS
})

const fetchError = (error:Error) => ({
    error,
    type: ERROR
})

const add = (payload: IDataPosts) => ({
    payload,
    type: ADD
})

const initialState = {
    data: {},
    fetching: false,
    fetched: false
}

export default function reducer( state = initialState, action:AnyAction ){
    switch (action.type) {
        case START:
            return {
                ...state,
                fetching: true
            }
        case SUCCESS:
            return {
                ...state,
                data: action.payload,
                fetching: false,
                fetched: true
            }
        case ERROR:
            return {
                ...state,
                error: action.error,
                fetching: false,
            }
        case ADD: 
            return {
                ...state,
                data: {
                    ...state.data,
                    ...action.payload,
                }
            }    

        default:
            return state;
    }   
}

export const fetchPosts = () => 
    async(dispatch: Dispatch, getState: () => any, { db, storage }:IServices)=>{
        dispatch(fetchStart())
        try {
            const snaps = await db.collection('posts').get()
            const posts:any = {}
            snaps.forEach(x => posts[x.id] = x.data())
            const imgIds = await Promise.all( Object.keys(posts)
                .map( async x => {
                    const ref = storage.ref(`posts/${x}.jpg`)
                    const url = await ref.getDownloadURL()
                    return [x, url]
                }))
            const keyedImages:any = {}
            imgIds.forEach( x => keyedImages[x[0]] = x[1])
            Object.keys(posts).forEach( x => posts[x] = {
                ...posts[x],
                imageURL: keyedImages[x]
            })
            dispatch(fetchSuccess(posts))
        }
        catch(e){
            dispatch(fetchError(e))
        }
    }

export const like = (id:string) => 
    async(dispatch: Dispatch, getState: ()=> any, { auth }:IServices) => {
       // tslint:disable-next-line:no-console
        console.log("like "+id)

        if(!auth.currentUser){
            return
        }
        const token = await auth.currentUser.getIdToken()

        const result = await fetch('/api/posts/'+id+'/like', {
            headers: {
                authorization: token
            }
        })
        const text = await result.text()        
        console.log(text)
    }

export const share = (id:string) => 
    async(dispatch: Dispatch, getState: ()=> any, { auth, db, storage }:IServices) => {
        // tslint:disable-next-line:no-console
        console.log("share "+id)
        if(!auth.currentUser){
            return
        }
        const token = await auth.currentUser.getIdToken()

        const result = await fetch('/api/posts/'+id+'/share', {
            headers: {
                authorization: token
            }
        })

        const url = await storage.ref(`posts/${id}.jpg`).getDownloadURL()
        const blob: any = await download(url);
        const { id: postId }: {id:string} = await result.json()

        const ref = await storage.ref( `posts/${postId}.jpg` )
        await ref.put(blob)
        const imageURL = await ref.getDownloadURL()
        const snap = await db.collection('posts').doc(postId).get()

        dispatch(add({ [snap.id]: {
            ...snap.data(),
            imageURL
        } } as IDataPosts))
    }


