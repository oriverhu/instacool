  import firebase from 'firebase';
// import * as firebase from 'firebase/app'
// import { firestore, storage as Storage, auth as Auth } from 'firebase';

const firebaseConfig = {
    apiKey: "****",
    authDomain: "****",
    databaseURL: "****",
    projectId: "****",
    storageBucket: "****",
    messagingSenderId: "****",
    appId:"****"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  // export const auth = Auth
  // export const db = firestore
  // export const storage = Storage
  
  export const auth = firebase.auth()
  export const db = firebase.firestore()
  export const storage = firebase.storage()