import * as React from 'react';

interface IIntroProps{
    text?: string
}
interface IIntroState{
    text: string,
    dato: number
}

export default class Intro extends React.Component<IIntroProps,IIntroState>{
    public state = {
        text: 'Soy un texto del estado',
        dato: 1 // si esto no está da un error
    }

    public render() {
        const { text } = this.props;
        const t = text ? text : this.state.text;

        return (
            <div onClick={this.handleClick} className="App-intro">      
                <h3>{t}</h3>        
            </div>     
        )
    }

    private handleClick = () => {
        this.setState({ text: 'Texto actualizado' })
    }
}