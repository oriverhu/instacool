import React from 'react';

const style = {
    backgroundColor: '#fff',
    border: '0px',
    padding: '10px 15px'
}

export default class Title extends React.Component{
    public render(){
        return(
            <h2 { ...this.props } style={style} />
        )
    }
}