import React from 'react';
import { reduxForm, InjectedFormProps, Field } from 'redux-form';
import { Link } from 'react-router-dom';
import { ILogin } from './../ducks/Users';

import Input from './Input';
import Button from './Button';
import Center from './Center';

const RegisterForm: React.FC<InjectedFormProps<ILogin>> = props => {
    const { handleSubmit } = props
        return(
        <form onSubmit={handleSubmit} >        
                 <Field component={Input} type="email" name="email"  placeholder="Email" label="Email" />
                 <Field component={Input} type="password" name="password"  placeholder="Clave" label="Clave"  />
                
                <Button block>Enviar</Button> 
                <Center>
                    <Link to='/'>Iniciar Sesión</Link>
                </Center>
        </form>
        )    
}

export default reduxForm<ILogin>({
    form: 'register'
})(RegisterForm)