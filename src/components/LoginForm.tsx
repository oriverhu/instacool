import React from 'react';
import { Link } from 'react-router-dom';
import { reduxForm, InjectedFormProps, Field } from 'redux-form';
import { ILogin } from './../ducks/Users';

import Input from './Input';
import Button from './Button';
import Center from './Center';

const LoginForm: React.FC<InjectedFormProps<ILogin>> = props => {
  
        const { handleSubmit } = props
        return(
            <form onSubmit={handleSubmit} >          
                {/* <Input placeholder="Correo" label="Correo" />
                <Input placeholder="Contraseña" label="Contraseña" /> */}
                 <Field component={Input} type="email" name="email"  placeholder="Email" label="Email" />
                 <Field component={Input} type="password" name="password"  placeholder="Clave" label="Clave"  />
                
                <Button block>Enviar</Button> 
                <Center>
                    <Link to='/register'>Ir al registro</Link>
                </Center>
            </form>
        )
    
}

export default reduxForm<ILogin>({
    form: 'login'
})(LoginForm)