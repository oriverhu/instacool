import React from 'react';
import { reduxForm, InjectedFormProps, Field, WrappedFieldProps, WrappedFieldInputProps } from 'redux-form';

const styles={
    img: {
        borderRadius:'100%',
        cursor: 'pointer',
        width: '100px',
        height: '100px'
    },
    file:{
        display:'none'
    }
}

interface IProfileImg {
    submitProfileImg: ()=>void,
    profileImage: string
}  

const handleChange = (submitProfileImg: () => void, input: WrappedFieldInputProps ) => async(e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault()
    const { onChange } = input
    const { files } = e.target
    if(files){
       await onChange(files[0])
        submitProfileImg()
    }
} 

const RenderField: React.StatelessComponent<WrappedFieldProps & IProfileImg> = ({ input, submitProfileImg,profileImage }) => 
    <div>
        {/* <input type="file" style={styles.file} {...input} id="profileImage" /> */}
        <input type="file" style={styles.file} id="profileImage" onChange={handleChange(submitProfileImg,input)} />
        <label htmlFor="profileImage">
             <img style={styles.img} src={profileImage} alt="Perfil"/>
        </label>
    </div>


class ProfileImg extends React.Component<InjectedFormProps<{}, IProfileImg> & IProfileImg>{
    public render(){
        const { handleSubmit, submitProfileImg, profileImage } = this.props
        return(
            <form onSubmit={handleSubmit} >
                <Field name="profileImg" component={RenderField} submitProfileImg={submitProfileImg} profileImage={profileImage}  />
            </form>
        )
    }
}

export default reduxForm<{}, IProfileImg>({
    form: 'profileImg'
})(ProfileImg)
